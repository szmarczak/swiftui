//
//  ContentView.swift
//  szmarczak
//
//  Created by Piotr on 14/06/2023.
//

import SwiftUI
import CoreData

struct Event: Identifiable, Hashable {
    var id = UUID()
    var hourStart: UInt8 = 0
    var minuteStart: UInt8 = 0
    var hourEnd: UInt8 = 0
    var minuteEnd: UInt8 = 0
    var name: String = ""
    var color: Color = .blue
    var important: Bool = false
    
    init(_ hourStart: UInt8 = 0, _ minuteStart: UInt8 = 0, _ hourEnd: UInt8 = 0, _ minuteEnd: UInt8 = 0, _ name: String = "", _ color: Color = .blue, _ important: Bool = false) {
        self.hourStart = hourStart
        self.minuteStart = minuteStart
        self.hourEnd = hourEnd
        self.minuteEnd = minuteEnd
        self.name = name
        self.color = color
        self.important = important
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func ==(lhs: Event, rhs: Event) -> Bool {
        return lhs.id == rhs.id
    }
}

struct AddRoute: Hashable {}
struct InfoRoute: Hashable {}

struct ContentView: View {
    @StateObject private var storage = Storage()

    @State var currentWeekday: Int = 2
    @State var titleColor: Color = .black

    @State private var path = NavigationPath()

    private var calendar = Calendar(identifier: .iso8601)

    init() {
        _currentWeekday = .init(initialValue: calendar.component(.weekday, from: Date()))
    }

    var body: some View {
        NavigationStack(path: $path) {
            VStack(alignment: .leading) {
                HStack() {
                    Image("Pencil").resizable().scaledToFit().frame(width: 40, height: 40)
                    Text("Timetable").font(.title).foregroundColor(titleColor)
                    Spacer()

                    Button(action: {
                        path.append(AddRoute())
                    }) {
                        Text("Add")
                    }
                }.padding([.leading, .trailing], 20).onTapGesture {
                    if titleColor == .black {
                        titleColor = .orange
                    } else {
                        titleColor = .black
                    }
                }.onLongPressGesture {
                    path.append(InfoRoute())
                }

                Picker("Weekday", selection: $currentWeekday) {
                    ForEach(1...7, id: \.self) { day in
                        Text(calendar.weekdaySymbols[day - 1])
                    }
                }.pickerStyle(.segmented).onChange(of: currentWeekday, perform: { newState in
                    storage.fetchWeekday(weekday: newState)
                })

                List(storage.items.sorted {
                    $0.hourStart < $1.hourStart || ($0.hourStart == $1.hourStart && $0.minuteStart < $1.minuteStart)
                }) {
                    item in
                    EventListItem(item: item, onEditButtonClick: {
                        path.append(item)
                    }, onDeleteButtonClick: {
                        storage.deleteItem(item: item)
                        storage.fetchWeekday(weekday: currentWeekday)
                    })
                }.listStyle(.plain).onAppear {
                    storage.fetchWeekday(weekday: currentWeekday)
                }

                Spacer()
            }.navigationDestination(for: Item.self) {
                item in
                SwiftUIView(day: calendar.weekdaySymbols[currentWeekday - 1], item: item, save: {
                    data in
                    storage.saveItem(weekday: currentWeekday, item: item, data: data)
                    storage.fetchWeekday(weekday: currentWeekday)
                    path.removeLast()
                })
            }.navigationDestination(for: AddRoute.self) {
                details in
                SwiftUIView(day: calendar.weekdaySymbols[currentWeekday - 1], item: nil, save: {
                    data in
                    storage.saveItem(weekday: currentWeekday, item: nil, data: data)
                    storage.fetchWeekday(weekday: currentWeekday)
                    path.removeLast()
                })
            }.navigationDestination(for: InfoRoute.self) {
                details in
                InfoView()
            }
        }
    }
}
