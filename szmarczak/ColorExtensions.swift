//
//  ColorExtensions.swift
//  szmarczak
//
//  Created by Piotr on 18/06/2023.
//

import SwiftUI

extension Color {
    func toInt() -> Int {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        UIColor(self).getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        return (Int(red * 255) << 16) | (Int(green * 255) << 8) | Int(blue * 255)
    }
    
    static func fromInt(_ int: Int) -> Color {
        let red = Double((int >> 16) & 0xFF) / 255.0
        let green = Double((int >> 8) & 0xFF) / 255.0
        let blue = Double(int & 0xFF) / 255.0
        
        return Color(red: red, green: green, blue: blue)
    }
}
