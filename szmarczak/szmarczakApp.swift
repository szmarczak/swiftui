//
//  szmarczakApp.swift
//  szmarczak
//
//  Created by Piotr on 14/06/2023.
//

import SwiftUI

@main
struct szmarczakApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
