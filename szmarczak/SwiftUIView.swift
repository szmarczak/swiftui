//
//  SwiftUIView.swift
//  szmarczak
//
//  Created by Piotr on 16/06/2023.
//

import SwiftUI

typealias SaveData = (timeStart: Date, timeEnd: Date, bgColor: Color, important: Bool, name: String)
typealias SaveCallback = (_ data: SaveData) -> Void

struct SwiftUIView: View {
    private var day: String
    
    var save: SaveCallback
    
    @State private var timeStart = Date()
    @State private var timeEnd = Date()
    @State private var bgColor = Color(.sRGB, red: 0.98, green: 0.9, blue: 0.2)
    @State private var important = false
    @State private var name = ""

    private var isValid: Bool {
        !name.isEmpty
    }

    init(day: String, item: Item?, save: @escaping SaveCallback) {
        self.day = day
        self.save = save
        
        if let item = item {
            _timeStart = .init(initialValue: Calendar.current.date(bySettingHour: Int(item.hourStart), minute: Int(item.minuteStart), second: 0, of: Date())!)
            _timeEnd = .init(initialValue: Calendar.current.date(bySettingHour: Int(item.hourEnd), minute: Int(item.minuteEnd), second: 0, of: Date())!)
            _bgColor = .init(initialValue: Color.fromInt(Int(item.color)))
            _important = .init(initialValue: item.important)
            _name = .init(initialValue: item.name)
        }
    }
    
    var body: some View {
        VStack {
            TextField(text: $name) {
                Text("Event name")
            }.textFieldStyle(.roundedBorder)
            
            HStack {
                Grid(alignment: .leading) {
                    GridRow {
                        Text("Time Start")

                        DatePicker(selection: $timeStart, displayedComponents: [.hourAndMinute]) {}.labelsHidden()
                    }
                    
                    GridRow {
                        Text("Time End")

                        DatePicker(selection: $timeEnd, displayedComponents: [.hourAndMinute]) {}.labelsHidden()
                    }
                    
                    GridRow {
                        Text("Color")

                        ColorPicker(selection: $bgColor, supportsOpacity: false) {}.labelsHidden()
                    }
                    
                    GridRow {
                        Text("Important")

                        Toggle(isOn: $important) {}.labelsHidden()
                    }
                }
                
                Spacer()
            }
            
            Spacer()
            
            Button(action: {
                save((
                    timeStart: timeStart,
                    timeEnd: timeEnd,
                    bgColor: bgColor,
                    important: important,
                    name: name
                ))
            }) {
                Text("Save")
            }.buttonStyle(.bordered).disabled(!isValid)
        }.padding().navigationTitle(day)
    }
}
