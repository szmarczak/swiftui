//
//  InfoView.swift
//  szmarczak
//
//  Created by Patryk Maciąg on 19/06/2023.
//

import SwiftUI

struct InfoView: View {
    @State var size: CGFloat = 50
    
    var body: some View {
        VStack {
            Spacer()
            
            Grid(alignment: .leading) {
                GridRow {
                    Text("Autor:")
                    Text("Szymon Marczak")
                }
                
                GridRow {
                    Text("Version:")
                    Text("1.0.0")
                }
            }
            
            Spacer()
            
            Image(systemName: "face.smiling.inverse").font(.system(size: size)).foregroundColor(.yellow).onTapGesture {
                size += 10
            }
        }
    }
}
