//
//  EventListItem.swift
//  szmarczak
//
//  Created by Piotr on 16/06/2023.
//

import SwiftUI

// https://www.w3.org/TR/WCAG20/#relativeluminancedef
func L(color: Color) -> Double {
    var r: CGFloat = 0
    var g: CGFloat = 0
    var b: CGFloat = 0
    var a: CGFloat = 0

    UIColor(color).getRed(&r, green: &g, blue: &b, alpha: &a)

    var red = Double(r)
    var green = Double(g)
    var blue = Double(b)

    if red <= 0.03928 {
        red /= 12.92
    } else {
        red = pow(((red + 0.055) / 1.055), 2.4)
    }

    if green <= 0.03928 {
        green /= 12.92
    } else {
        green = pow(((green + 0.055) / 1.055), 2.4)
    }

    if blue <= 0.03928 {
        blue /= 12.92
    } else {
        blue = pow(((blue + 0.055) / 1.055), 2.4)
    }

    return 0.2126 * red + 0.7152 * green + 0.0722 * blue
}

func textColor(bgColor: Color) -> Color {
    if L(color: bgColor) > 0.179 {
        return .black
    }

    return .white
}

struct EventListItem: View {
    var item: Item
    var onEditButtonClick: () -> Void
    var onDeleteButtonClick: () -> Void

    let inset = EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)

    func formattedTime(_ hour: Int16, _ minute: Int16) -> String {
        let formattedHour = hour < 10 ? "0\(hour)" : "\(hour)"
        let formattedMinute = minute < 10 ? "0\(minute)" : "\(minute)"
        return "\(formattedHour):\(formattedMinute)"
    }

    private var color: Color {
        Color.fromInt(Int(item.color))
    }

    var body: some View {
        HStack {
            VStack {
                Text(formattedTime(item.hourStart, item.minuteStart))
                Text(formattedTime(item.hourEnd, item.minuteEnd))
            }

            Text(item.name)

            Spacer()

            if item.important {
                Image(systemName: "exclamationmark.square.fill")
            }
        }
        .listRowInsets(inset)
        .padding(10)
        .background(color)
        .foregroundColor(textColor(bgColor: color))

        .swipeActions(edge: .trailing, content: {
            Button(role: .destructive, action: onDeleteButtonClick) {
                Label("Delete", systemImage: "trash")
            }.tint(.black)
        })
        .swipeActions(edge: .leading, content: {
            Button(action: onEditButtonClick) {
                Label("Edit", systemImage: "pencil.line")
            }.tint(Color.brown)
        })
    }
}
