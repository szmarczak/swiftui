//
//  Storage.swift
//  szmarczak
//
//  Created by Patryk Maciąg on 19/06/2023.
//

import Foundation
import CoreData

class Storage: ObservableObject {
    private var viewContext = PersistenceController.shared.container.viewContext
    @Published var items: [Item] = []
    
    func getDay(day: Int16) -> Day {
        let fetchRequest: NSFetchRequest<Day> = Day.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "day == %d", day)
        
        let result = try? viewContext.fetch(fetchRequest)
        
        if let result = result?.first {
            return result
        }
        
        let d = Day(context: viewContext)
        d.day = day
        
        do {
            try viewContext.save()
        } catch {
            fatalError(error.localizedDescription)
        }
        
        return d
    }
    
    func fetchWeekday(weekday: Int) {
        items = getDay(day: Int16(weekday)).itemSet
    }
    
    func saveItem(weekday: Int, item: Item?, data: SaveData) {
        let it = item ?? Item(context: viewContext)
        let calendar = Calendar(identifier: .iso8601)

        it.name = data.name
        it.hourStart = Int16(calendar.component(.hour, from: data.timeStart))
        it.minuteStart = Int16(calendar.component(.minute, from: data.timeStart))
        it.hourEnd = Int16(calendar.component(.hour, from: data.timeEnd))
        it.minuteEnd = Int16(calendar.component(.minute, from: data.timeEnd))
        it.color = Int32(data.bgColor.toInt())
        it.important = data.important
        
        getDay(day: Int16(weekday)).addToItem(it)
        
        do {
            try viewContext.save()
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func deleteItem(item: Item) {
        viewContext.delete(item)

        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}
