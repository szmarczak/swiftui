//
//  Day+CoreDataProperties.swift
//  szmarczak
//
//  Created by Piotr on 19/06/2023.
//
//

import Foundation
import CoreData


extension Day {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Day> {
        return NSFetchRequest<Day>(entityName: "Day")
    }

    @NSManaged public var day: Int16
    @NSManaged public var item: NSSet?
    
    public var itemSet: [Item] {
        let setOrArray = item as? Set<Item> ?? []
        
        return Array(setOrArray)
    }

}

// MARK: Generated accessors for item
extension Day {

    @objc(addItemObject:)
    @NSManaged public func addToItem(_ value: Item)

    @objc(removeItemObject:)
    @NSManaged public func removeFromItem(_ value: Item)

    @objc(addItem:)
    @NSManaged public func addToItem(_ values: NSSet)

    @objc(removeItem:)
    @NSManaged public func removeFromItem(_ values: NSSet)

}

extension Day : Identifiable {

}
