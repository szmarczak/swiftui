//
//  Item+CoreDataProperties.swift
//  szmarczak
//
//  Created by Piotr on 19/06/2023.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var color: Int32
    @NSManaged public var hourEnd: Int16
    @NSManaged public var hourStart: Int16
    @NSManaged public var important: Bool
    @NSManaged public var minuteEnd: Int16
    @NSManaged public var minuteStart: Int16
    @NSManaged public var name: String
    @NSManaged public var day: Day?

}

extension Item : Identifiable {

}
